import React from 'react';
import './pageproduct.css';
import { product, reviews } from '../../scripts/data';
import Header from './Header/Header.jsx';
import Breadcrumbs from './Breadcrumbs/Breadcrumbs';
import Gallery from './Gallery/Gallery';
import ColorPalet from './ColorPalet/ColorPalet';
import MemoryPalet from './MemoryPalet/MemoryPalet';
import Characteristics from './Characteristics/Characteristics';
import Description from './Description/Description';
import Reviews from './Reviews/Rewiews/Reviews';
import Ads from './Ads/Ads.jsx';
import Form from './Form/Form';
import PriceAndDeliveriSidebar from './PriceAndDeliveriSidebar/PriceAndDeliveriSidebar';
import Footer from './Footer/Footer';
//

function PageProduct() {
  return (
	<>
	<div className="container">
		<div className="container__conten-centring">
			<Header />
			<div className="product-wrapper">
				<Breadcrumbs />
				<div className="product-wrapper__product-name">
					<span className="product-wrapper__product-name_headline">{product.name}, {product.colorPicked}</span>
					<Gallery
						enColor = "red"
						rusColor = "красный"
					/>
				</div>
				<PriceAndDeliveriSidebar 
					lessOrEqual361 = "lessOrEqual361"				
				/>
				<main>
					<div className="product-information">
						<div>
							<section className="product-information__characteristics">
								<ColorPalet />
								<MemoryPalet />
								<Characteristics />
							</section>
								<Description />
							<div className="comparing-block">
								<section className="comparing-block__table">
									<h3 className="table__headline">Сравнение моделей</h3>
									<table className="table__compare">
                    <tbody>
                      <tr className="table__compare_header">
                        <th className="table__compare_cell table__compare_Head-cell">Модель</th>
                        <th className="table__compare_cell table__compare_Head-cell">Вес</th>
                        <th className="table__compare_cell table__compare_Head-cell">Высота</th>
                        <th className="table__compare_cell table__compare_Head-cell">Ширина</th>
                        <th className="table__compare_cell table__compare_Head-cell">Толщина</th>
                        <th className="table__compare_cell table__compare_Head-cell">Чип</th>
                        <th className="table__compare_cell table__compare_Head-cell">Объём памяти</th>
                        <th className="table__compare_cell table__compare_Head-cell">Аккумулятор</th>
                      </tr>
                      <tr className="table__compare_rows">
                        <td className="table__compare_cell">Iphone11</td>
                        <td className="table__compare_cell">194 грамма</td>
                        <td className="table__compare_cell">150.9 мм</td>
                        <td className="table__compare_cell">75.7 мм</td>
                        <td className="table__compare_cell">8.3 мм</td>
                        <td className="table__compare_cell">A13 Bionicchip</td>
                        <td className="table__compare_cell">до 128 Гб</td>
                        <td className="table__compare_cell">До 17 часов</td>
                      </tr>
                      <tr className="table__compare_rows">
                        <td className="table__compare_cell">Iphone12</td>
                        <td className="table__compare_cell">164 грамма</td>
                        <td className="table__compare_cell">146.7 мм</td>
                        <td className="table__compare_cell">71.5 мм</td>
                        <td className="table__compare_cell">7.4 мм</td>
                        <td className="table__compare_cell">A14 Bionicchip</td>
                        <td className="table__compare_cell">до 256 Гб</td>
                        <td className="table__compare_cell">До 19 часов</td>
                      </tr>
                      <tr className="table__compare_rows">
                        <td className="table__compare_cell">Iphone13</td>
                        <td className="table__compare_cell">174 грамма</td>
                        <td className="table__compare_cell">146.7 мм</td>
                        <td className="table__compare_cell">71.5 мм</td>
                        <td className="table__compare_cell">7.65 мм</td>
                        <td className="table__compare_cell">A15 Bionicchip</td>
                        <td className="table__compare_cell">до 512 Гб</td>
                        <td className="table__compare_cell">До 19 часов</td>
                      </tr>
                    </tbody>
									</table>
								</section>
							</div>
							<Reviews
								title={reviews.title}
								count={reviews.count}
								items={reviews.items}
              />
							<Form />
						</div>
						<div className="product-information__sidebar">
							<PriceAndDeliveriSidebar 
								lessOrEqual361 = "moreThan361"
							/>
							{/* <div className="sidebar__priceANDdelivery">
								<div className="priceANDdelivery__aboveheadline">
									<span>
										<span className="priceANDdelive__price_cross">75 990₽</span>
										<span className="priceANDdelive__price_discount">-8%</span>
									</span>
									<label>
										<input className="priceANDdelivery__heartSVGicon_radio" type="radio" name="heart" value="small"/>
										<svg className="heartSVGicon" width="28" height="22" viewBox="0 0 28 22"
											xmlns="http://www.w3.org/2000/svg">
											<path fillRule="evenodd" clipRule="evenodd"
												d="M2.78502 2.57269C5.17872 0.27474 9.04661 0.27474 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.27474 22.8216 0.27474 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63044 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63044 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" />
										</svg>
									</label>
								</div>
								<span className="priceANDdelivery__main-headline">
									67990₽
								</span>
								<div className="priceANDdelivery__delivery">
									<span className="priceANDdelivery__delivery-terms">
										Самовывоз в четверг, 1 сентября — <b>бесплатно</b></span>
									<span className="priceANDdelivery__delivery-terms">
										Курьером в четверг, 1 сентября — <b>бесплатно</b></span>
								</div>
								<button className="priceANDdelivery__basket-add-button">
									<span className="priceANDdelivery__basket-add-button_text">
										Добавить&nbsp;в&nbsp;корзину
									</span>
								</button>
							</div> */}
							<Ads />
						</div>
					</div>
				</main>
			</div>
			
		</div>
	
  </div>
	<Footer />
	</>);
}

export default PageProduct;
