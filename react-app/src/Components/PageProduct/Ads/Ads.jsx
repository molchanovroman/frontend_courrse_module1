import React from 'react';
import AdsFrame from './AdsFrame';
import './ads.css';

function Ads() {
  return (
    <div className="sidebar__advertisment">
    <div className="sidebar__advertisment_header">Реклама</div>
      <AdsFrame />
      <AdsFrame />
</div>
  )
}

export default Ads;