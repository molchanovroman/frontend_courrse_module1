import React from 'react';
import { product } from '../../../scripts/data';
import './list.css';

function List(props) {
  const { items } = props;
  return (
      <ul className="characteristics-block__list">
          {items.map(item => {
              return (
                  <li className="characteristics-block__list_items" key={item.title}>
                          <span className="characteristics-block__list_item_nowrap">{item.title}</span>
                          {item.elements.map((el, i) => {
                              return (
                                  el.isLink ? 
                                      <a
                                          className="link"
                                          href="https://ru.wikipedia.org/wiki/Apple_A15"
                                          rel="noreferrer noopener"
                                          target="_blank"
                                          key={`link-${el.value}`}
                                      >
                                          <span className='characteristics-block__list_bold'>{el.value}</span>
                                      </a> :
                                      <span className='characteristics-block__list_bold' key={el.value}>
                                          {el.value}{i === item.elements.length - 1 ? '' : ', '}
                                      </span>
                              );
                          })}
                  </li>
              );
          })}
      </ul>
  );
}

export default List;
