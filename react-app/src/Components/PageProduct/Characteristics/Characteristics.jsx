import React from 'react';
import { product } from '../../../scripts/data';
import './characteristics.css';
import List from './List';

function Characteristics() {
  const { charcs } = product.properties;
  return (
    <div className="product-information__characteristics-block">
									<h3 className="characteristics-block__headline_NOmargin">
										{charcs.title}
									</h3>
                  <List items={charcs.items}/>
		</div>
  )
}
export default Characteristics;