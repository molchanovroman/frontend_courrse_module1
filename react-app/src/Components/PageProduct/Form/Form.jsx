import React from "react";
import { useRef, useEffect } from 'react';
import './form.css';

function Form() {
  const ref = useRef(null);
  useEffect(() => {
    {
      let scoreErrorDiv;
      let nameErrorDiv;
      let scoreInputError;
      let nameInputError;
      let submited = 0;
      class Forms {
        constructor(form) {
          this.form = form;
        }
          dataRestore = function() {
            let storedFieldsClassesRestore=JSON.parse(localStorage.getItem("storedFieldsClasses"));
            handleDataRestore();
            function handleDataRestore() {
              if (storedFieldsClassesRestore !== null) {
                for (let i=0; i < storedFieldsClassesRestore.length; i++) {
                  let restoringField = storedFieldsClassesRestore[i];
                  document.querySelector(`.${restoringField}`).value = localStorage.getItem(restoringField);
                }
              }
            }
          }
          formDataStore = function() {
            let forms = this.form;
            document.addEventListener("keyup", handleFormType);
            /* Универсальный объект сохранения данных введённых 
            в формы вне зависимости от их количества на странице 
            Сбор всех форм на странице в NodeList и извлечение их
            классов для последующего сбора классов input-ов*/
            let formClassesArr = [];
            forms.forEach(forms => {
              let formClasses = forms.classList;
              formClassesArr.push(formClasses[0]);
            })
            //Сбор классов хранимых полей
            let storedFieldsClasses = [];
            let formClassToFetchInputClasses;
            for (let i=0; i < formClassesArr.length; i++) {
              formClassToFetchInputClasses = formClassesArr[i];
              let inputClasses = document.querySelector(`.${formClassToFetchInputClasses}`);
              let inputs = inputClasses.querySelectorAll('input');
                //Помещение классов в массив для дальнейшего пополнения 
                inputs.forEach(inputs => {
                  let inputClassesList = inputs.classList;
                  storedFieldsClasses.push(inputClassesList[0]);
                })
              //Пополнение массива хранимых полей
              let textarea = inputClasses.querySelectorAll('textarea');
                textarea.forEach(textarea => {
                  let textareaClasses = textarea.classList;
                  //console.log(textareaClasses[0]);
                  storedFieldsClasses.push(textareaClasses[0]);
                })
            }
            //Запись данных полей ввода в localStorage
            function handleFormType() {
              for (let i=0; i < formClassesArr.length; i++) {
                let formClassTypeE = formClassesArr[i];
                localStorage.setItem("storedFieldsClasses",JSON.stringify(storedFieldsClasses));
                //console.log(`.${formClassTypeE}`);
                form = document.querySelector(`.${formClassTypeE}`);
                let formData = new FormData(form);
                //Получение ключей форм
                for (const key of formData.keys()) {
                  //console.log(key);
                  //Получение значений ключей для сохранения по ним введённых данных
                  if ((formData.get(`${key}`)).length > 0) {
                    //console.log(formData.get(`${key}`));
                    localStorage.setItem(`${key}`, (formData.get(`${key}`)));
                  } 
                }
              }
            }
          }
          SuccessedSubmit(submited) {
            if (submited === 1) {
              alert("Ваш отзыв был успешно отправлен и будет отображён после модерации");
            }
          }
      }
  
      class AddReviewForm extends Forms {
        Submiting = function() {
          document.addEventListener("submit", handleSubmit);
          function handleSubmit(e) {
            e.preventDefault();
            let formClick = document.querySelector(".new-review__input-form");
            formClick.addEventListener("click", formErrorHide);
            let inputedName = document.querySelector(".name");
            let name = inputedName.value;
            let inputedScore = document.querySelector(".rating");
            let score = +inputedScore.value;
            let validation = 0;
            scoreErrorDiv = document.querySelector(".score_error_message");
            nameErrorDiv = document.querySelector(".name_error_message");
            scoreInputError = document.querySelector(".rating");
            nameInputError = document.querySelector(".name");
            if (validation === 0) {
              if ( name.length < 2 && name.length !== 0 ) {
                nameInputError.classList.add("error_border");
                nameErrorDiv.classList.add("name_error");
                nameErrorDiv.innerHTML = "Имя не может быть меньше 2-х символов";
                e.preventDefault();
              } else if (name.length === 0) {
                nameInputError.classList.add("error_border");
                nameErrorDiv.classList.add("name_error");
                nameErrorDiv.innerHTML = "Вы забыли указать имя и фамилию";
                e.preventDefault();
              } 
              else if (score < 1 || score > 5) {
                scoreInputError.classList.add("error_border");
                scoreErrorDiv.classList.add("score_error");
                scoreErrorDiv.classList.add("error_border");
                e.preventDefault();
                scoreErrorDiv.innerHTML = "<span> Оценка должна быть <br> от 1 до 5</span>";
              } 
              else if (typeof(score) !== "number" || isNaN(score) === true) {
                scoreInputError.classList.add("error_border");
                scoreErrorDiv.classList.add("score_error");
                scoreErrorDiv.classList.add("error_border");
                console.log(score);
                scoreErrorDiv.innerHTML = "<span> Оценка должна быть <br> от 1 до 5</span>";
                e.preventDefault();
              } 
              else {
                validation = 1;
                localStorage.clear();
                formClick.reset();
                submited = 1;
                reviewform.SuccessedSubmit(submited);
                form.submit();
              }
            }
          }
          function formErrorHide() {
            console.log("ErrorHiding");
            scoreInputError = document.querySelector(".rating");
            nameInputError = document.querySelector(".name");
            nameErrorDiv = document.querySelector(".name_error_message");
            scoreErrorDiv = document.querySelector(".score_error_message");
            if (nameInputError.value.length > 0) {
              nameInputError.classList.remove("error_border");
              scoreInputError.classList.remove("error_border");
              scoreErrorDiv.classList.remove("score_error");
              nameErrorDiv.classList.remove("name_error");
            }
          }
        }
      }
      let form = new Forms(document.querySelectorAll("form"));
      form.dataRestore();
      form.formDataStore();
      let reviewform = new AddReviewForm(document.querySelector(".new-review__input-form"));
      reviewform.Submiting();
    };
});
return (
  <div className="form__wrapper">
    {/* <div>
        <img className="reviews__review_reviewerFoto hidden" src="/img/review-1.jpeg" alt="Фото рецензента" />
    </div> */}
    <div className="new-review">
      <form className="new-review__input-form" name="myForm" action="#" method="get">
        <legend className="new-review__input-form__caption">Добавить свой отзыв</legend>
        <fieldset className="new-review__input-form__fields">
          <div className="new-review__input-form__head">
            <div className="new-review__input-form__head_name" ref={ref}>
              <input className="input-form__input_name input-form__input name" type="text"
                id="input-form__input_name" name="input-form__input_name" placeholder="Имя и Фамилия"/>
              <div className="name_error_message">123</div>
            </div>
            <div className="new-review__input-form__head_score">
              <input className="input-form__input_rating input-form__input rating" id="input-form__input_rating"
                name="input-form__input_rating" placeholder="Оценка"/>
              <div className="score_error_message">123</div>
            </div>
          </div>
          <textarea className="input-form__input_textarea input-form__input review"
            id="input-form__input_textarea" name="input-form__input_textarea" placeholder="Текст отзыва"
            rows="11"></textarea>
          <button type="submit" className="input-form__submit-button" value="Отправить отзыв">Отправить
            отзыв</button>
        </fieldset>
      </form>
    </div>
  </div>
);
}

export default Form;