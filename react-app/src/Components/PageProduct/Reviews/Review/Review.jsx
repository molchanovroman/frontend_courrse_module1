import Rating from '../Rating/Rating';
import './review.css';

function Review(props) {
    const { cn, info: { author, rating, details }  } = props;
    return (
        <>
        <div className={`${cn}`}>
            <div>
                <img className="reviews__review_reviewerFoto" src={author.img} alt={author.alt} />
            </div>
            <div>
                <div>
                    <h4 className="review__text-block_reviewerName">{author.name}</h4>
                    <Rating cn="review__rating" score={rating} />
                </div>
                <div className="review__text-block__text">
                    {details.map((detail, index) => {
                        return (
                            <p
                                className="review__text-block-content_text"
                                key={detail.value}
                            >
                                <span className='review__text-block-content_text_bold'>{detail.title}</span>
                                {index !== 0 && <br />}
                                {detail.value}
                            </p>
                        );
                    })}
                </div>
            </div>
        </div>
        <hr className='reviews__hr'/>
        </>
    );
}

export default Review;