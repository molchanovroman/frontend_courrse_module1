import React from "react";
import './rating.css';

function Rating(props) {
  const { cn, score } = props;
  const arrScore = Array(5).fill(1).fill(0, score);
  return (
      <div className={`${cn}`}>
          {arrScore.map((item, index) => {
              return (
                  <div key={index}>
                      {item ?
                          <img className="review__rating_star" src="img/star.svg" alt="Полная звезда" /> :
                          <img className="review__rating_star" src="img/star-empty.svg" alt="Пустая звезда" />
                      }
                  </div> 
              );
          })}
      </div>
  );
}

export default Rating;