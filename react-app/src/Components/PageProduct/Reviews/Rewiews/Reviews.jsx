import React from 'react';
import Review from '../Review/Review';
import './reviews.css';
function Reviews(props) {
    const { title, count, items } = props;
    return (
        <section className="reviews">
            <div className="reviews__header_nameANDcount">
                <span className="reviews__nameANDcount_headline">{title}</span>
                <span className="reviews__nameANDcount_count">{count}</span>
            </div>
            <div>
                {items.map((item) => {
                    return <Review cn="reviews__review" info={item} key={item.author.id}/>;
                })}
            </div>
        </section>
    );
}

export default Reviews;