import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Error404 from '../Error404/Error404';

function PageNotFound() {
  return(
    <div className="container">
      <div className="container__conten-centring">
        <Header />
      </div>
        <Error404 />
        <Footer />
    </div>
  )
}

export default PageNotFound;