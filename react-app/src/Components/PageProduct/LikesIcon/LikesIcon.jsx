import React from 'react';
import './likesicon.css';

function LikesIcon() {
  return(
    <div className="header__like">
								<img className='heartico' src='./img/heart.svg' alt='Сердечко'></img>
								<div className="like__content">
									<div className="like__content_count">
										'12'
									</div>
								</div>
							</div>
  )
}

export default LikesIcon;