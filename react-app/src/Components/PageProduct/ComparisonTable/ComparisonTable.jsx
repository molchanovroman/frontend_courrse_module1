import React, { Component } from 'react';  
import ReactTable from "react-table";  
  
class ComparisonTable extends Component {  
  render() {  
     const data = [
      {model:'iPhone 11', 
      weight:'194 грамма',
      height:'150.9 мм',
      width:'75.7 мм',
      thickness:'8.3 мм',
      chipset:'A13 Bionicchip',
      memory:'до 128 Гб',
      onbatterylifetime:'до 17 часов'},
      {model:'iPhone 12', 
      weight:'164 грамма',
      height:'146.7 мм',
      width:'71.5 мм',
      thickness:'7.4 мм',
      chipset:'A14 Bionicchip',
      memory:'до 256 Гб',
      onbatterylifetime:'до 19 часов'}
  ]  
     const columns = [
      {  
      Header: 'Модель',  
      accessor: 'model'  
      },{  
      Header: 'Вес',  
      accessor: 'weight'  
      },{  
      Header: 'Высота',  
      accessor: 'height'  
      },{  
      Header: 'Ширина',  
      accessor: 'width'  
      },{  
      Header: 'Толщина',  
      accessor: 'thickness'  
      },{  
      Header: 'Чип',  
      accessor: 'chipset'  
      },{  
      Header: 'Объём памяти',  
      accessor: 'memory'  
      },{  
      Header: 'Аккумулятор',  
      accessor: 'onbatterylifetime'  
      }]   
    return (  
          <div>  
              <ReactTable  
                  data={data}  
                  columns={columns}  
                  defaultPageSize = {2}  
                  pageSizeOptions = {[1,2,3]}  
              />  
          </div>        
    )  
  }  
}  
export default ComparisonTable; 