import React from "react";
import './priceanddeliverisidebar.css';

function PriceAndDeliveriSidebar(props) {
  const { lessOrEqual361 } = props;
  return(
    <div className={`sidebar__priceANDdelivery ${lessOrEqual361}`}>
      <div className="priceANDdelivery__aboveheadline">
        <span>
          <span className="priceANDdelive__price_cross">75 990₽</span>
          <span className="priceANDdelive__price_discount">-8%</span>
        </span>
        <label>
          <input className="priceANDdelivery__heartSVGicon_radio" type="radio" name="heart" value="small"/>
          <svg className="heartSVGicon" width="28" height="22" viewBox="0 0 28 22"
            xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd"
              d="M2.78502 2.57269C5.17872 0.27474 9.04661 0.27474 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.27474 22.8216 0.27474 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63044 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63044 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" />
          </svg>
        </label>
      </div>
      <span className="priceANDdelivery__main-headline">
        67990₽
      </span>
      <div className="priceANDdelivery__delivery">
        <span className="priceANDdelivery__delivery-terms">
          Самовывоз в четверг, 1 сентября — <b>бесплатно</b></span>
        <span className="priceANDdelivery__delivery-terms">
          Курьером в четверг, 1 сентября — <b>бесплатно</b></span>
      </div>
      <button className="priceANDdelivery__basket-add-button">
        <span className="priceANDdelivery__basket-add-button_text">
          Добавить&nbsp;в&nbsp;корзину
        </span>
      </button>
		</div>
  )
}

export default PriceAndDeliveriSidebar;