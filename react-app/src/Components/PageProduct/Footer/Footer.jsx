import React from 'react';
import './footer.css';

function Footer() {
  return(
    <footer className="footer">
			<div className='footer361'>
				<div className="footer__kontakts">
					<span className="kontakts__name">© ООО «<span className="footer_First-name-word">Мой</span>Маркет»,
						2018-2022.</span>
					<span className="kontakts__detailed-info">Для уточнения информации звоните по номеру
						<a className="kontakts__detailed-info_link footer__links" href="tel:+79000000000">
							+7 900 000 0000,</a>
					</span>
						<span className="kontakts__colaboration">а предложения по сотрудничеству отправляйте на&nbsp;почту
							<a className="kontakts__colaboration_link footer__links" href="email:partner@mymarket.com">
							partner@mymarket.com
							</a>
						</span>
				</div>
				<div className="footer__anker">
					<p><a className="footer__anker_NOunderline footer__links" href="#top">Наверх</a></p>
				</div>
			</div>
			</footer>
  )
}

export default Footer;