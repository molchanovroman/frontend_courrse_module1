import { useState, useEffect } from 'react';
import React from 'react';
import { product } from '../../../scripts/data';
import './colorpalet.css';
//

function ColorPalet() {
  const { properties } = product;
    let [activeColorBtn, setActiveColorBtn] = useState(3);
    useEffect(() => {
      let SelectedColorName = properties.color.items[activeColorBtn].color;
      let SelectedPalletColorDOM = document.querySelector(".characteristics-block__headline_NOmargin_color");
      SelectedPalletColorDOM.innerHTML = `Цвет товара: ${SelectedColorName}`;
      let SelectedColorPageTop = document.querySelector(".product-wrapper__product-name_headline");
      SelectedColorPageTop.innerHTML = `Смартфон Apple iPhone 13, ${SelectedColorName}`;
    });
  return (
    <>
    <h3 className="characteristics-block__headline_NOmargin_color">
      Цвет товара: {product.colorPicked}
    </h3>
    <div className="characteristics-block__product-palette">
        {properties.color.items.map((item, index) => {
            const active = index === activeColorBtn;
            let activeClass = active ? 'product-palette__item_selected' : '';
            return (
                  <div className={ `product-palette__item ${activeClass}`}
                  onClick={() => {
                    return setActiveColorBtn(index);
                  }}
                  key={item.src}
                  >
                    <div className="characteristics-block__product-palette">
                      <img src={item.src} alt={item.alt} 
                      className="characteristics-block__product-palette_IsmallLook"/>
                    </div>
                    </div>
            );
        }
        )
        }
    </div>
  </>
  )
}

export default ColorPalet;