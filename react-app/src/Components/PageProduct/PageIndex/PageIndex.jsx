import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import PageIndexPlug from '../PageIndexPlug/PageIndexPlug';

function PageIndex() {
  return(
    <div className="container">
      <div className="container__conten-centring">
        <Header />
      </div>
        <PageIndexPlug />
        <Footer />
    </div>
  )
}

export default PageIndex;