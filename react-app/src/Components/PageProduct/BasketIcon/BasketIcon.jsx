import React from 'react';
//import { useState } from 'react';
import './basketicon.css';

function BasketIcon() {
  //let {count, setCount} = useState(1);
  return(
    <div className="header__basket">
                <img className='basketico' src='./img/basket.svg' alt='Иконка корзины'></img>
								<div className="basket__content">
									<div className="basket__content_count">
										3
									</div>
								</div>
							</div>
  )
}

export default BasketIcon;