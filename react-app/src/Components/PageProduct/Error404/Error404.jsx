import React from "react";
import './error404.css';

function Error404() {
  return (
    <div className="error404">
          <p>Error 404 page not found</p>
    </div>
  )
}

export default Error404;