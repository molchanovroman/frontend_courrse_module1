import {breadcrumbsItems} from '../../../scripts/data';
import './breadcrumbs.css';
//

function Breadcrumbs(props) {
  return (
      <nav className="nav-menue">
          {breadcrumbsItems.map(breadcrumbsItems => {
              return (
                    <a key={breadcrumbsItems.id} 
                    className="link nav-menue__item" 
                    href={breadcrumbsItems.url}
                    >
                      {breadcrumbsItems.name}
                    </a>
              );
          })}
      </nav>
  );
}

export default Breadcrumbs;