import React from "react";
import { product } from '../../../scripts/data';
//import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import './gallery.css';

function Gallery() {
  //let { hidden, setHidden } =useState(0)
  return (
      <div className="product-wrapper__product-name_product-views">
        {product.gallery.items.map((item, index) => {
          const hidden = index;
          let hiddenClass = hidden > 1 ? `IlookHidden` : '';
        return (
              <div key = {item.id}>
                <img src={item.src} alt={item.alt} 
                className={`Ilook ${hiddenClass}`} />
              </div>
    );
}
)
}
    </div>
  )
}

export default Gallery;