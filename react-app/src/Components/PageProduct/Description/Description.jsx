import React from "react";
import './description.css';
import { product } from '../../../scripts/data';

function Description() {
  const { description } = product.properties;
  return(
    <section className="poduct-description">
      <h3 className="poduct-description__headline">{description.title}</h3>
      <div className="poduct-description__text">
        <div className="features">
         {description.features.map((features, i) => {
            return (
                i !== description.features.length - 1 ? 
                (<p key={features.id} className="nomarginparagraph" >{features.text}</p>)
                : (<p key={features.id} className="nomarginparagraph tagline" >{features.text}</p>)
            );
        })}
        </div>
        <div className="paragraphs"> 
          {description.paragraphs.map((paragraphs, i) => {
            return (
                i !== description.paragraphs.length - 1 ? 
                (<p key={paragraphs.id} className="nomarginparagraph" >{paragraphs.text}</p>)
                : (<p key={paragraphs.id}>{paragraphs.text}</p>)
            );
        })}
        </div>
      </div>
    </section>
  )
}

export default Description;