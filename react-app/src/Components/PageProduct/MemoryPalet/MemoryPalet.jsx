import { useState, useEffect } from 'react';
import React from 'react';
import { product } from '../../../scripts/data';
import './memorypalet.css';

function MemoryPalet() {
  const { properties } = product;
    let [activeRamBtn, setActiveRamBtn] = useState(0);
    useEffect(() => {
      document.querySelector(".characteristics-block__headline_NOmargin").innerHTML 
      = `${product.properties.ram.title} ${product.properties.ram.items[activeRamBtn]}`;
    });
  return(
    <div className="product-information__characteristics-block">
      <h3 className="characteristics-block__headline_NOmargin">
        {product.properties.ram.title} {product.ramPicked}
      </h3>
      <div className="characteristics-block__product-palette">
        {properties.ram.items.map((item, index) => {
            const active = index === activeRamBtn;
            let activeClass = active ? 'product-information__characteristics_memory_item_selected' : '';
            return (
                  <div className={ `product-information__characteristics_memory_item ${activeClass}`}
                  onClick={() => {
                    return setActiveRamBtn(index);
                  }}
                  key={item}
                  >
                      <span>{item}</span>
                    </div>
            );
        }
        )
        }
    </div>
    </div>
    
  )
}
export default MemoryPalet;