import React from 'react';
import { Link } from 'react-router-dom';
import './pageindexplug.css';

function PageIndexPlug() {
  return(
      <div className="pageindex__plug">
            <center className="pageindex__plug__text">Здесь должно быть содержимое главной страницы.
               Но в рамках курса главная страница  используется лишь
               в демонстрационных целях
               <Link className='pageindex__plug__link' to={"./product"}>
                <div><span>Перейти на страницу товара</span></div>
              </Link>
            </center>
      </div>
  )
}

export default PageIndexPlug;