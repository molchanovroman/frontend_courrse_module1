"use strict";
// Exercise 1
console.log("Exercize 1");
for(let i=0;i<=20;++i){
    (i%2!=1)?console.log(i):i;
}
// Exercise 2
console.log("Exercize 2");
let intermediateValue=0;
let number;
for (let i = 1; i <= 3; ++i){
    number = prompt("Для расчёта суммы 3-х чисел введите " + i + " число");
    if (isNaN(number)){
        alert("Ошибка, вы ввели "+'"'+ number.toString() +'"'+" это не число")
    break;
    }
    else{intermediateValue = intermediateValue + +number;}
}
isNaN(number)?number:console.log(intermediateValue);
// Exercise 3
console.log("Exercize 3");
let month;
function getNameOfMonth(month){
    if(isNaN(month)){}else{month=+month;}
    switch(month){
        case 0:
        console.log("Январь");
        break;
        case 1:
        console.log("Февраль");
        break;
        case 2:
        console.log("Март");
        break;
        case 3:
        console.log("Апрель");
        break;
        case 4:
        console.log("Май");
        break;
        case 5:
        console.log("Июнь");
        break;
        case 6:
        console.log("Июль");
        break;
        case 7:
        console.log("Август");
        break;
        case 8:
        console.log("Сентябрь");
        break;
        case 9:
        console.log("Октябрь");
        break;
        case 10:
        console.log("Ноябрь");
        break;
        case 11:
        console.log("Декабрь");
        break;
        default: 
        console.log("Номер месяца может быть только числом в диапазоне от 0 до 11, вы ввели " + month.toString());
    }
}
month = prompt("Введите номер месяца от 1 до 12");
console.log("Вы выбрали: ");
getNameOfMonth(month);
console.log("Названия месяцев кроме Октября:");
for(month=0;month<=11;++month){
    if(month!=9) {getNameOfMonth(month)} }
// Exercise 4
/* IIFE (Immediately Invoked Function Expression) это JavaScript 
функция, которая выполняется сразу же после того, как она была определена.
(function () {
    statements
})();
Это тип выражений, также известный как Self-Executing Anonymous Function, 
который состоит из двух основных частей. Первая - это сама анонимная функция 
с лексической областью видимости, заключённым внутри Оператора группировки (). 
Благодаря этому переменные IIFE замыкаются в его пределах, и глобальная 
область видимости ими не засоряется.
Вторая часть создаёт мгновенно выполняющееся функциональное выражение () , 
благодаря которому JavaScript-движок выполняет функцию напрямую.
Переменная, которой присвоено IIFE, 
хранит в себе результат выполнения функции, но не саму функцию.
var result = (function () {
    var name = "Barry";
    return name;
})(); */
