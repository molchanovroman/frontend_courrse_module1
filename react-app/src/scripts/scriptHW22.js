"use strict";
// Exercise 1
let arr1 = [1,2,10,5];
let arr2 = ["a", {},3,3, -2];
function getSumm(arr) {
    let summ = 0;
    for (let key in arr) {
        if (typeof arr[key] === "number") summ += arr[key];
    }
    return summ;
}
console.log("arr1 contain: " + arr1);
console.log("Sum arr1's numeric keys is " + getSumm(arr1));
console.log("arr2 contain: " + arr2);
console.log("Sum arr2's numeric keys is " + getSumm(arr2));
// Exercise 3
function addToCart(productId) {
    if (cart.every(function(n)
        {return n !== productId;
        })) cart.push(productId);
        return cart;
}
function removeFromCart(productId) {
    let i= cart.indexOf(productId);
    cart = cart.filter((productId, index) => index !== i);
    return cart;
}
let cart= [4884];
// Adding an item
addToCart(3456);
console.log(cart);
// [4884, 3456]
// Adding the same item again
addToCart(3456);
console.log(cart);
// [4884, 3456]
// Deleting item
removeFromCart(4884);
console.log(cart);
// [3456]
