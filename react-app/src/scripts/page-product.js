let form = document.querySelector(".new-review__input-form");
let inputedName = form.querySelector(".name");
let inputedScore = form.querySelector(".rating");
let submitButton = document.querySelector(".input-form__submit-button");
let scoreErrorDiv;
let nameErrorDiv;
let scoreInputError;
let nameInputError;
let localStorageCount = localStorage.getItem("basketCount");
let basketContent = document.querySelector(".basket__content");
let basketAddButtonText = document.querySelector(".priceANDdelivery__basket-add-button_text");
scoreInputError = document.querySelector(".rating");
nameInputError = document.querySelector(".name");
nameErrorDiv = document.querySelector(".name_error_message");
scoreErrorDiv = document.querySelector(".score_error_message");
//
function handleSubmit(event) {
  event.preventDefault();
  let name = inputedName.value;
  let score = +inputedScore.value;
  let validation = 0;
  if (validation === 0) {
    if ( name.length < 2 && name.length != 0 ) {
      nameInputError.classList.add("error_border");
      nameErrorDiv.classList.add("name_error");
      nameErrorDiv.innerHTML = "Имя не может быть меньше 2-х символов";
    } else if (name.length === 0) {
      nameInputError.classList.add("error_border");
      nameErrorDiv.classList.add("name_error");
      nameErrorDiv.innerHTML = "Вы забыли указать имя и фамилию";
    } else if (score < 1 || score > 5) {
      scoreInputError.classList.add("error_border");
      scoreErrorDiv.classList.add("score_error");
      scoreErrorDiv.classList.add("error_border");
      event.preventDefault();
      scoreErrorDiv.innerHTML = "<span> Оценка должна быть <br> от 1 до 5</span>";
    } else if (typeof(score) !== "number" || isNaN(score) === true) {
      scoreInputError.classList.add("error_border");
      scoreErrorDiv.classList.add("score_error");
      scoreErrorDiv.classList.add("error_border");
      scoreErrorDiv.innerHTML = "<span> Оценка должна быть <br> от 1 до 5</span>";
      event.preventDefault();
    } else {
      validation = 1;
      localStorage.removeItem("name");
      localStorage.removeItem("rating");
      localStorage.removeItem("review");
      document.querySelector(".name").value = "";
      document.querySelector(".rating").value = "";
      document.querySelector(".review").value = "";
      form.submit();
    }
  }
}
function handleFormClick() {
  if (nameInputError.value.length > 0) {
  nameInputError.classList.remove("error_border");
  scoreInputError.classList.remove("error_border");
  scoreErrorDiv.classList.remove("score_error");
  nameErrorDiv.classList.remove("name_error");
  }
}
function handleFormType() {
  let tempStoreNameInput = form.querySelector(".name").value;
  localStorage.setItem("name", tempStoreNameInput);
  let tempStoreScoreInput = form.querySelector(".rating").value;
  localStorage.setItem("rating", tempStoreScoreInput);
  let tempStoreReviewInput = form.querySelector(".review").value;
  localStorage.setItem("review", tempStoreReviewInput);
}
function dataRestore(){
  let restoredFormInputedName = localStorage.getItem("name");
  let restoredFormInputedScore = localStorage.getItem("rating");
  restoredFormInputedScore = restoredFormInputedScore;
  let restoredFormInputedReview = localStorage.getItem("review");
  document.querySelector(".name").value = restoredFormInputedName;
  document.querySelector(".rating").value = restoredFormInputedScore;
  document.querySelector(".review").value = restoredFormInputedReview;
  if (localStorageCount == null) {
  localStorage.setItem("basketCount", '0');
  localStorageCount = +localStorage.getItem("basktCount");
  } else if (localStorageCount > 0) {
    basketContent.classList.add("basket__content_added");
    document.querySelector(".basket__content_count").innerHTML = localStorageCount;
    basketAddButton.classList.add("basket-add-button_used");
    basketAddButtonText.innerHTML = "Товар&nbsp;уже&nbsp;в&nbsp;корзине";
    basketAddButton.classList.remove("priceANDdelivery__basket-add-button");
  }
}
function basketFilling(){
  if(+localStorage.getItem("basketCount") == 0) {
    basketContent.classList.add("basket__content_added");
    basketAddButtonText.innerHTML = "Товар&nbsp;уже&nbsp;в&nbsp;корзине";
    basketAddButton.classList.remove("priceANDdelivery__basket-add-button");
    basketAddButton.classList.add("basket-add-button_used");
    localStorage.setItem ("basketCount", ++localStorageCount);
  } else if (+localStorage.getItem("basketCount") !== 0) { 
      localStorage.setItem ("basketCount", --localStorageCount);
      basketContent.classList.remove("basket__content_added");
      basketContent.classList.add("basket__content");
      basketAddButton.classList.remove("basket-add-button_used");
      basketAddButton.classList.add("priceANDdelivery__basket-add-button");
      basketAddButtonText.innerHTML = "Добавить&nbsp;в&nbsp;корзину";
  }
  document.querySelector(".basket__content_count").innerHTML = localStorageCount;
}
form.addEventListener("submit", handleSubmit);
form.addEventListener("click", handleFormClick);
form.addEventListener("keyup", handleFormType);
document.addEventListener('DOMContentLoaded',dataRestore);
let basketAddButton = document.querySelector(".priceANDdelivery__basket-add-button");
basketAddButton.addEventListener("click", basketFilling);
