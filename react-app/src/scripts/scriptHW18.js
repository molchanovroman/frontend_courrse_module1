"use strict";
//Упражнение 1
let a = '100px';
let b = '323px';
let result = parseInt(a) + parseInt(b);
console.log(result);
//Упражнение 2
 console.log(Math.max(10, -45, 102, 36, 12, 0, -1));
 //Упражнение 3
 let c = 123.3399;
 console.log(Math.round(c));
 let d = 0.111;
 console.log(Math.ceil(d));
 let e = 45.333333;
 console.log(e.toFixed(1));
 let f = 3;
 console.log(f**5);
 let g = 4e14;
 console.log(g.toExponential());
 let h = '1' == 1;
 console.log(h);
//Упражнение 4
/* The problem is lieing in non accurate storing numbers 0.1&0.2 in binary system.
0.1 not exactly 0.1 it is periodic fraction 0.0001100110011001100110011001100110011001100110011001101
and the same for 0.2: 0.001100110011001100110011001100110011001100110011001101
when we summarise 0.1 and 0.2 in binary system common for number storing in JavaScript,
we summarise their mathematical errors in binary system. 
In the end we are compearing 0.30000000000000004 and 0.3. 
They are not equal. 
We can fix that proble by rounding summarising result */
let sum = 0.1+0.2;
 console.log(sum === 0.3); //false
//упражнение 4(fix)
let sumFix = 0.1+0.2;
 console.log(+sumFix.toFixed(1) === 0.3); //true
