"use strict";
// Exercise 1
let obj = {};
let NotEmptyobj = {1:1,2:2,3:3};
/**
 * Checks if the object is empty 
 * @param {object} a checked object
 * @param {boolean} isEmpty returned answer is object empty or not
*/
function isEmpty(a){
    let isEmpty = Object.keys(a).length === 0;
    return isEmpty;
}
console.log(obj);
console.log("\"Object obj is empty\" statement is " + isEmpty(obj));
console.log(NotEmptyobj);
console.log("\"Object NotEmptyobj is empty\" statement is " + isEmpty(NotEmptyobj));

// Exercise 3
let salaries = {
    John:100000,
    Ann:160000,
    Pete:130000,
};
let percent = prompt("input salary modification percent");
percent = 1 + percent/100;
/** 
 * Modifies salary value
 * @param {object} salaries contains each employee now salary value
 * @param {number} percent user inputted value of salary modificator
 */
function rizeSalary(){
Object.keys(salaries).forEach(key => {
  salaries[key] = Math.floor(salaries[key]*percent);
});
};
rizeSalary();
let budget = 0;
for (let key in salaries) {
    budget= budget + salaries[key];
};
console.log("The total value of all salaries after modification to " 
            + Math.floor((percent-1)*100) + "%: " + budget);