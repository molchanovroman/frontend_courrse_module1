"use strict";
// Exercise 1
function countDown() {
  let n= +prompt("Input positive number of seconds for countDown");
  let m = n;
  if (Number.isNaN(n) === true) {
  alert("You input not a number, please, reload the page and try again");}
  else if(n <= 0) {
    alert("Number should be positive and not equal 0, "+
    "please, reload the page and try again");}
  else {
    alert("Allright you've inputed a " + typeof(n) +" " + n);
  let intervalId = setInterval(() => {
    console.log("Remains " + n + " sec.");
    --n;
    if (n === 0) {
      clearInterval(intervalId);
      if (m > 1) {
      console.log(m + " seconds have left ");}
      else {
      console.log(m + " second has left");}
    }
  }, 1000);
  }
  }
  // Exercise 2
  function apiFetch() {
    let fetchedData;
    let promise = fetch("https://reqres.in/api/users");
    promise
    .then(function(response) {
      return response.json();
    })
    .then(function (response) {
      let users = response.data;
      console.log("Recieved users: " + users.length);
      Object.keys(users).forEach(key => {
        console.log(
        "-" + users[key].first_name +
        " " + users[key].last_name +
        " (" + users[key].email + ")");
      });
    })
    .catch(function() {
      console.log("Кажется бэкенд сломался :(");
    });
  }
// I tried to find a way to run countDown first but couldn't 
apiFetch();
countDown();



