import React from "react";
import {BrowserRouter,Routes,Route} from "react-router-dom";
import PageIndex from '../src/Components/PageProduct/PageIndex/PageIndex';//"../components/PageIndex/PageIndex";
import PageProduct from "../src/Components/PageProduct/PageProduct";
import PageNotFound from "../src/Components/PageProduct/PageNotFound/PageNotFound";

function App() {
return (
// Эти два компонента обязательно нужно добавить,
// чтобы роутинг в приложении работал корректно
<BrowserRouter>
  <Routes>
    <Route path="/" element={<PageIndex/>}/>
    {/* Параметр path указываем по какому адресубудут доступна эта страница */}
    <Route path="/product" element={<PageProduct/>}/>
    {/* Если человек ввел другой адрес, то показываемстраницу 404 */}
    <Route path="*" element={<PageNotFound/>}/>
  </Routes>
</BrowserRouter>
    );}

export default App;
